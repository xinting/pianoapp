package com.xinting.piano.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xinting.piano.R;
import com.xinting.piano.data.PUser;
import com.xinting.piano.service.PMessageService;

/**
 * Created by wuxinting on 15/11/14.
 * 用户私信页面
 */
public class PUserMessageFragment extends Fragment {

    private PUser user;

    public PUser getUser() {
        return user;
    }

    public PUserMessageFragment setUser(PUser user) {
        this.user = user;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        PMessageService messageService = new PMessageService();
        messageService.pullMessage();
    }
}
