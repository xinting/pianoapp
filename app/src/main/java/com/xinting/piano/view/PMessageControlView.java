package com.xinting.piano.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;

/**
 * Created by wuxinting on 15/11/15.
 * 信件的控件
 */
public class PMessageControlView implements ICustomControlView {

    @Override
    public int getType() {
        return PApp.CONTROL_VIEW_MESSAGE;
    }

    @Override
    public void draw(Canvas canvas, float w, float h, Paint paint) {
        paint.setColor(PColor.METAL_RED);

        //信封面
        canvas.drawRect(w / 4, h / 3, w * 3 / 4, h * 2 / 3, paint);

        //信封舌三角边沿
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);
        canvas.drawLine(w / 4, h / 3, w / 2, h / 2, paint);
        canvas.drawLine(w / 2, h / 2, w * 3 / 4, h / 3, paint);
    }
}
