package com.xinting.piano.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;

/**
 * Created by wuxinting on 15/10/29.
 */
public class PAlbumView extends PTimerView {

    private Paint paint;
    private Path textPath;
    private Context context;

    private String author;
    private String title;

    private static float angle;

    private final int INVALID_INTERVAL = 40;

    public PAlbumView(Context context) {
        super(context);
        initialize(context);
    }

    public PAlbumView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        this.context = context;

        paint = new Paint(Color.RED);
        paint.setAntiAlias(true);

        textPath = new Path();

        //set interval of invalidate
        setInterval(INVALID_INTERVAL);
    }

    public void startRoll() {
        start();
    }

    public void stopRoll() {
        stop();
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //get the smaller one
        int len = widthMeasureSpec > heightMeasureSpec ? heightMeasureSpec : widthMeasureSpec;
        Log.v(PApp.LOG_TAG, "Album size: " + len);
        super.onMeasure(len, len);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        boolean roll = isStarted();
        float len = canvas.getWidth();
        float radius = len*0.9f;
        if (roll) {
            canvas.rotate(angle, len/2, len/2);
        }

        paint.setColor(PColor.METAL_BLACK);
        canvas.drawCircle(len/2, len/2, radius/2, paint);

        paint.setColor(PColor.METAL_RED);
        canvas.drawCircle(len/2, len/2, radius/4.5f, paint);

        if (getAuthor() != null && !getAuthor().isEmpty()) {
            textPath.reset();
            textPath.addCircle(len / 2, len / 2, radius / 3f, Path.Direction.CW);
            paint.setColor(Color.WHITE);
            paint.setTextSize(20);

            //以后文字正好在顶部2*pi*r/4 * 3 - (lenofstr)/2
            String toDraw = getTitle()+"--"+getAuthor();
            float[] widths = new float[toDraw.length()+1];
            paint.getTextWidths(toDraw, widths);
            float toDrawLen = 0.0f;
            for (int i = 0; i < widths.length; i++) {
                toDrawLen += widths[i];
            }
            canvas.drawTextOnPath(getTitle()+"--"+getAuthor(), textPath, (float) (Math.PI*radius/2f-toDrawLen/2f), 0, paint);
        }

        if (roll) {
            canvas.rotate(-angle, len/2, len/2);
        }
        angle += 0.5;
    }
}
