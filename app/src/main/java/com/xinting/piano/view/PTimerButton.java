package com.xinting.piano.view;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

/**
 * Created by wuxinting on 16/1/9.
 */
public class PTimerButton extends Button {

    // 递增计数
    private boolean bAscend;

    // 最小时间 默认为0s
    private long min;

    // 最大时间 默认为60s
    private long max;

    // 刷新的间隔
    private int interval;

    // 正在刷新
    private boolean bStarted;

    private long current;

    // 显示的格式
    private String format;

    private String text;
    private Handler handler;
    private Runnable runnable;
    private Runnable myRunnable;

    public PTimerButton(Context context) {
        super(context);
        initialize();
    }

    public PTimerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    private void initialize() {
        setAscend(false);
        setMin(0);
        setMax(60);
        setInterval(1);
        setFormat("%d");

        current = isAscend() ? min : max;

        myRunnable = new Runnable() {
            @Override
            public void run() {
                if (runnable != null) {
                    runnable.run();
                }

                String text = String.format(format, current);
                setText(text);

                if (isAscend()) {
                    current += interval;
                } else {
                    current -= interval;
                }

                synchronized (this) {
                    if (interval > 0 && current > min && current < max) {
                        handler.postDelayed(this, interval*1000);
                    } else {
                        setText(PTimerButton.this.text);
                        stop();
                        current = isAscend() ? min : max;
                    }
                }
            }
        };

        handler = new Handler(Looper.getMainLooper());

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isStarted()) {
                    start();
                }
            }
        });

        // 保留原来的显示字符串
        text = getText().toString();
    }

    /**
     * 设置显示的格式
     * @param format 格式串
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * 开始计数，按格式设置button上的显示字符串
     */
    public void start() {
        handler.post(this.myRunnable);
        setStarted(true);
        setTextColor(getResources().getColor(android.R.color.darker_gray));
    }

    /**
     * 停止计数
     */
    public void stop() {
        handler.removeCallbacks(myRunnable);
        setStarted(false);
        setTextColor(getResources().getColor(android.R.color.white));
    }

    public boolean isAscend() {
        return bAscend;
    }

    public void setAscend(boolean bAscend) {
        this.bAscend = bAscend;
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public String getFormat() {
        return format;
    }

    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getInterval() {
        return interval;
    }

    public boolean isStarted() {
        return bStarted;
    }

    public void setStarted(boolean bStarted) {
        this.bStarted = bStarted;
    }
}
