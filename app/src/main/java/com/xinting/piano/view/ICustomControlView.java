package com.xinting.piano.view;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by wuxinting on 15/11/8.
 * 自定义的control类型
 * PControlView.addCustomControlView()
 */
public interface ICustomControlView {

    /**
     * 该control对应的类型，当PControlView.setType()时调用与之相同type自定义control
     * > 0
     * @return
     */
    int getType();

    /**
     * 绘制自定义control
     * @param canvas 画布
     * @param paint 画笔
     * @param w 控件宽
     * @param h 控件高
     */
    void draw(Canvas canvas, float w, float h, Paint paint);
}
