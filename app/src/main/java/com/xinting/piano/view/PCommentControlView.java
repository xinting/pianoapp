package com.xinting.piano.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;

/**
 * Created by wuxinting on 15/11/8.
 */
public class PCommentControlView implements ICustomControlView {

    private Path path;

    public PCommentControlView() {
        path = new Path();
    }

    @Override
    public int getType() {
        return PApp.CONTROL_VIEW_COMMENT;
    }

    @Override
    public void draw(Canvas canvas, float w, float h, Paint paint) {
        paint.setColor(PColor.METAL_RED);

        //兼容android 9中没有drawOval函数
        canvas.scale(1, 0.9f);
        canvas.drawCircle(w / 2, h / 2, (w > h ? h : w) * 0.5f / 2, paint);

        //画小三角
        path.reset();
        path.moveTo(w*0.5f, h*0.85f);
        path.lineTo(w*0.3f, h*0.5f);
        path.lineTo(w*0.7f, h*0.5f);
        path.lineTo(w*0.5f, h*0.85f);
        canvas.drawPath(path, paint);

        canvas.scale(1, 1.1f);
    }
}
