package com.xinting.piano.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;

/**
 * Created by wuxinting on 15/11/26.
 * 心形（喜欢）
 */
public class PLikeControlView implements ICustomControlView {

    private Path path;

    public PLikeControlView() {
        path = new Path();
    }

    /**
     * 画心形
     * @param size 正方形的区域中画心
     */
    private Path updatePath(float size) {
        path.reset();

        float unit = size / 5;
        path.moveTo(unit, 0);
        for (float i = 0; i < Math.PI*2; i += 0.1) {
            float x = (float) (unit*(Math.cos(i)+Math.sin(2*i)/2));
            float y = (float) (unit*(Math.sin(i)-Math.cos(2*i)/2+0.5));
            if (y > 0) {
                x -= Math.sin(i*2)*unit/5;
            }
//            float x = (float) (unit * (16 * Math.pow(Math.sin(i), 3)));
//            float y = (float) (unit * ((13 * Math.cos(-i)) - (5 * Math.cos(2 * -i)) - (2 * Math.cos(3 * -i)) - Math.cos(4 * -i)));
            path.lineTo(x, y);
        }
        path.offset(size / 2, size / 3);

        return path;
    }

    @Override
    public int getType() {
        return PApp.CONTROL_VIEW_LIKE;
    }

    @Override
    public void draw(Canvas canvas, float w, float h, Paint paint) {
        paint.setColor(PColor.METAL_RED);

        Path path = updatePath(w > h ? h : w);

        canvas.drawPath(path, paint);
    }
}
