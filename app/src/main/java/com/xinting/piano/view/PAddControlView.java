package com.xinting.piano.view;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;

/**
 * Created by wuxinting on 15/11/15.
 * 添加的『十』字控件
 */
public class PAddControlView implements ICustomControlView {

    @Override
    public int getType() {
        return PApp.CONTROL_VIEW_ADD;
    }

    @Override
    public void draw(Canvas canvas, float w, float h, Paint paint) {
        paint.setColor(PColor.METAL_RED);

        float size = w > h ? h : w;
        paint.setStrokeWidth(5);
        canvas.drawLine(size / 4, size / 2, size * 3 / 4, size / 2, paint);
        canvas.drawLine(size / 2, size / 4, size / 2, size * 3 / 4, paint);
    }
}
