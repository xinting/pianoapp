package com.xinting.piano.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;

/**
 * Created by wuxinting on 15/11/14.
 * 自定义的用户图标
 */
public class PUserControlView implements ICustomControlView {

    private Path path;

    public PUserControlView() {
        path = new Path();
    }

    /**
     * 根据给的size，更新半圆
     * @param x,y 圆心
     * @param r 半径
     * @return
     */
    private Path updatePath(float x, float y, float r) {
        path.reset();

        float j;
        path.moveTo(x-r, y);
        for (float i = x-r; i < x+r; i += 1) {
            //i是横坐标，j是纵坐标
            j = y - (float) Math.sqrt(r*r - (i-x)*(i-x));
            path.lineTo(i, j);
        }
        path.lineTo(x+r, y);
        path.lineTo(x-r, y);

        return path;
    }

    @Override
    public int getType() {
        return PApp.CONTROL_VIEW_USER;
    }

    @Override
    public void draw(Canvas canvas, float w, float h, Paint paint) {
        paint.setColor(PColor.METAL_RED);

        float size = (w > h ? h : w);

        //画圆
        canvas.drawCircle(w / 2, h / 3, size * 0.5f / 3, paint);

        //画肩
//        canvas.drawRect(size / 4, size / 2, size * 3 / 4, size * 3 / 4, paint);
        Path path = updatePath(size / 2, size * 3 / 4, size / 4);
        canvas.drawPath(path, paint);
    }
}
