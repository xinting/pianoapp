package com.xinting.piano.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by wuxinting on 15/11/14.
 * 用于在view中附加一个数据体
 */
public class PDataView extends TextView {

    private Object data;

    public PDataView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
