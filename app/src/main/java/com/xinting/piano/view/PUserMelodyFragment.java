package com.xinting.piano.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.xinting.piano.R;
import com.xinting.piano.app.PAlbumActivity;
import com.xinting.piano.app.PMelodyAdapter;
import com.xinting.piano.app.PTagAdapter;
import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PMelody;
import com.xinting.piano.data.PUser;
import com.xinting.piano.lib.util.PUtil;
import com.xinting.piano.service.IHandleJson;
import com.xinting.piano.service.PAlbum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuxinting on 15/11/14.
 * 用户发布的乐曲
 */
public class PUserMelodyFragment extends Fragment {

    public static final String LOG_TAG = PApp.LOG_TAG+"_"+PUser.KEY;
    private PUser user;

    private PAlbum album;
    private ListView melodyListView;
    private PMelodyAdapter adapter;
    private List<PMelody> melodies;

    public PUserMelodyFragment() {
        album = new PAlbum(null);
        melodies = new ArrayList<>();
        adapter = new PMelodyAdapter(PApp.getAppContext(), melodies);
        adapter.setTagAdapter(new PTagAdapter(PApp.getAppContext()));
    }

    public PUser getUser() {
        return user;
    }

    public PUserMelodyFragment setUser(PUser user) {
        this.user = user;

        if (user != null) {
            Log.v(LOG_TAG, user.getNick());
        } else {
            Log.v(LOG_TAG, "null user");
        }
        album.updateMelodies(user, null, new HandleJson());

        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_melody, container, false);

        melodyListView = (ListView) v.findViewById(R.id.user_melody_list);
        melodyListView.setAdapter(adapter);
        melodyListView.setOnItemClickListener(new OnMelodyItemClickListener());

        return v;
    }

    /**
     * 乐曲项被点击事件
     */
    private class OnMelodyItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent();
            Object object = adapterView.getItemAtPosition(i);
            if (object instanceof PMelody) {
                intent.putExtra(PMelody.KEY, (Serializable) object);
            }
            intent.setClass(PApp.getAppContext(), PAlbumActivity.class);
            startActivity(intent);
        }
    }

    /**
     * 处理返回
     */
    private class HandleJson extends Handler implements IHandleJson {

        @Override
        public void handleJson(JSONObject json) {
            Message msg = Message.obtain();
            msg.obj = json;
            sendMessage(msg);
        }

        @Override
        public void handleMessage(Message msg) {
            JSONObject json = (JSONObject) msg.obj;

            try {
                if (json.has(PApp.ERR_CODE)) {
                    int code = json.getInt(PApp.ERR_CODE);
                    if (code != PApp.ERR_SUCCESS) {
                        PUtil.toast(PApp.getAppContext(), json.getString(PApp.ERR_MSG));
                        return;
                    }

                    if (json.has(PMelody.KEY_S)) {
                        JSONArray array = json.getJSONArray(PMelody.KEY_S);
                        for (int i = 0; i < array.length(); i++) {
                            PMelody melody = new PMelody();
                            melody.fromJson(array.getJSONObject(i));
                            melodies.add(melody);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage() + ":" +json.toString());
            }
        }
    }
}
