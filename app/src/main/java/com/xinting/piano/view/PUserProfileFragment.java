package com.xinting.piano.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.xinting.piano.R;
import com.xinting.piano.app.PMessageActivity;
import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PMessage;
import com.xinting.piano.data.PUser;

/**
 * Created by wuxinting on 15/11/14.
 * 用户个人信息页面
 */
public class PUserProfileFragment extends Fragment {

    private PUser user;
    private PControlView messageControlView;
    private ICustomControlView customControlView;

    private LayoutInflater inflater;

    public PUser getUser() {
        return user;
    }

    public PUserProfileFragment setUser(PUser user) {
        this.user = user;
        customControlView = new PMessageControlView();
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_user, container, false);
        this.inflater = inflater;

        if (user != null) {
            fillTableByUserProfile(user, (TableLayout) v.findViewById(R.id.user_profile));
        }

        messageControlView = (PControlView) v.findViewById(R.id.user_control_message);
        messageControlView.addCustomControlView(customControlView);
        messageControlView.setCustomType(PApp.CONTROL_VIEW_MESSAGE);
        messageControlView.setOnClickListener(new OnMessageClickListener());

        return v;
    }

    /**
     * fill user profile to the table layout dynamically
     * @param user the user
     * @param layout the table
     */
    private void fillTableByUserProfile(PUser user, TableLayout layout) {
        if (inflater == null) {
            return;
        }

        layout.addView(fillTableRow(R.string.user_nick_label, user.getNick()));

        if (!TextUtils.isEmpty(user.getPhone())) {
            layout.addView(fillTableRow(R.string.user_phone_label, user.getPhone()));
        }
    }

    /**
     * 直充一行
     * @param id label string id
     * @param v value
     * @return table row view
     */
    private TableRow fillTableRow(int id, String v) {
        TableRow row = (TableRow) inflater.inflate(R.layout.fragment_user_profile_row, null, false);
        TextView label = (TextView) row.getChildAt(0);
        TextView value = (TextView) row.getChildAt(1);

        label.setText(getResources().getString(id));
        value.setText(v);
        return row;
    }

    @Override
    public void onResume() {
        super.onResume();

        /**
         * 可能在android机制中，新的activity覆盖之后，释放了该内容（未知）
         */
        messageControlView.addCustomControlView(customControlView);
    }

    /**
     * 发送信件的点击
     */
    private class OnMessageClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setClass(PApp.getAppContext(), PMessageActivity.class);
            if (user != null) {
                intent.putExtra(PUser.KEY, user);
            }
            intent.putExtra(PApp.KEY_ACTION, PMessage.ACTION_ADD);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PApp.getAppContext().startActivity(intent);
        }
    }

}
