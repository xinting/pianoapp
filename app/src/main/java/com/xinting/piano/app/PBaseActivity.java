package com.xinting.piano.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hannesdorfmann.swipeback.Position;
import com.hannesdorfmann.swipeback.SwipeBack;
import com.xinting.piano.data.PApp;

/**
 * Created by wuxinting on 15/10/28.
 */
public class PBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PApp.setAppContext(this.getApplicationContext());
    }

    /**
     * 获取对应类型的view
     * @param id
     * @param <T>
     * @return
     */
    protected <T> T $(int id) {
        return (T) findViewById(id);
    }

    protected String $s(int id) {
        return getResources().getString(id);
    }
}
