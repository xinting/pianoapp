package com.xinting.piano.app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by wuxinting on 15/11/14.
 * 屏幕滑动的adapter
 */
public class PScreenSlideAdapter extends FragmentStatePagerAdapter {

    /**
     * 滑动的屏幕
     */
    private List<Fragment> fragments;

    public PScreenSlideAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
