package com.xinting.piano.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hannesdorfmann.swipeback.Position;
import com.hannesdorfmann.swipeback.SwipeBack;
import com.xinting.piano.R;
import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PMessage;
import com.xinting.piano.data.PUser;
import com.xinting.piano.service.PAccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by wuxinting on 15/11/30.
 */
public class PAboutActivity extends PBaseActivity {

    private static final String ABOUT_TXT = "about.txt";

    private Button feedbackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SwipeBack.attach(this, Position.LEFT)
            .setContentView(R.layout.activity_about)
            .setSwipeBackView(R.layout.swipeback_default);

        feedbackBtn = $(R.id.about_feedback);

        feedbackBtn.setOnClickListener(new FeedBackClick());

        initialize();
    }

    /**
     * 展示about中的内容
     */
    private void initialize() {
        TextView content = $(R.id.about_content);

        BufferedReader bufferedReader = null;
        try {
            InputStream aboutis = getResources().getAssets().open(ABOUT_TXT);
            InputStreamReader reader = new InputStreamReader(aboutis);
            bufferedReader = new BufferedReader(reader);
            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }

            content.setText(sb.toString());
        } catch (IOException e) {
            Log.e(PApp.LOG_TAG, e.toString());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {

                }
            }
        }
    }

    /**
     * 点击反馈
     */
    private class FeedBackClick implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            if (PAccount.getInstance().getStatus() != PAccount.Status.LOGIN) {
                intent.setClass(PAboutActivity.this, PApplyLoginActivity.class);
                intent.putExtra(PApp.KEY_NEXT, PAboutActivity.class.getName());
                startActivity(intent);

                finish();
            } else {
                intent.setClass(PAboutActivity.this, PMessageActivity.class);

                PUser me = new PUser();
                me.setId("0");
                me.setNick("author");
                intent.putExtra(PUser.KEY, me);
                intent.putExtra(PApp.KEY_ACTION, PMessage.ACTION_ADD);
                startActivity(intent);
            }
        }
    }

}
