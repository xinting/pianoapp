package com.xinting.piano.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ListView;

import com.xinting.piano.R;
import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PComment;
import com.xinting.piano.data.PMelody;
import com.xinting.piano.service.IHandleJson;
import com.xinting.piano.service.PCommentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuxinting on 15/11/10.
 * 显示所有评论的activity
 */
public class PCommentActivity extends PBaseActivity {

    public static final String LOG_TAG = PApp.LOG_TAG+"_comment";

    /**
     * 针对哪个melody的评论
     */
    private PMelody melody;
    private PCommentService commentService;

    private List<PComment> comments;
    private ListView commentListView;
    private PCommentAdapter adapter;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Intent intent = getIntent();
        melody = (PMelody) intent.getSerializableExtra(PMelody.KEY);

        comments = new ArrayList<>();
        commentListView = $(R.id.comment_comments);

        handler = new CommentsHandler();

        adapter = new PCommentAdapter(this, comments);
        adapter.setUserAdapter(new PUserAdapter(this));
        commentListView.setAdapter(adapter);

        if (melody != null) {
            commentService = new PCommentService(melody);
        }

        commentService.getMelodyComments(new HandleComments());
    }

    /**
     * 更新UI用的handler
     */
    private class CommentsHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            JSONObject json = (JSONObject) msg.obj;
            Log.v(LOG_TAG, json.toString());
            try {
                int code = json.getInt(PApp.ERR_CODE);
                if (code != PApp.ERR_SUCCESS) {
                    Log.e(LOG_TAG, json.getString(PApp.ERR_MSG));
                }

                JSONArray array = json.getJSONArray(PCommentService.KEY_COMMENTS);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject commentJson = array.getJSONObject(i);
                    PComment comment = new PComment();
                    comment.fromJson(commentJson);

                    comments.add(comment);
                }
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                Log.e(LOG_TAG, json.toString());
            }
        }
    }

    /**
     * 处理服务器回传的评论JSON
     */
    private class HandleComments implements IHandleJson {

        @Override
        public void handleJson(JSONObject json) {
            Message msg = Message.obtain();
            msg.obj = json;
            handler.sendMessage(msg);
        }
    }

}
