package com.xinting.piano.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.TextView;

import com.hannesdorfmann.swipeback.Position;
import com.hannesdorfmann.swipeback.SwipeBack;
import com.xinting.piano.R;
import com.xinting.piano.data.PApp;

/**
 * Created by wuxinting on 15/11/27.
 * 系统公告activity
 */
public class PAnnounceActivity extends PBaseActivity {

    private TextView contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SwipeBack.attach(this, Position.LEFT)
            .setContentView(R.layout.activity_announce)
            .setSwipeBackView(R.layout.swipeback_default);

        Intent intent = getIntent();

        contentView = $(R.id.announce_content);

        if (intent.hasExtra(PApp.KEY_CONTENT)) {
            contentView.setText(intent.getStringExtra(PApp.KEY_CONTENT));
        }
    }

}
