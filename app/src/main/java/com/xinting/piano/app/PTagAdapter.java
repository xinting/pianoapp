package com.xinting.piano.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xinting.piano.R;
import com.xinting.piano.data.PTag;

import java.util.List;

/**
 * Created by wuxinting on 15/10/28.
 */
public class PTagAdapter {

    private Context context;

    private LayoutInflater inflater;
    private static final int resource = R.layout.tags_item;
    private static final int id = 0x7f041002;

    public PTagAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(List<PTag> tags, LinearLayout parent) {
        if (tags == null) return null;

        LinearLayout layout;
        layout = (LinearLayout) parent.findViewById(id);
        if (layout == null) {
            layout = (LinearLayout) inflater.inflate(resource, parent, false);
            layout.setId(id);
            parent.addView(layout);
        }

        for (int i = 0; i < layout.getChildCount(); i++) {
            if (i >= tags.size()) {
                layout.getChildAt(i).setVisibility(View.GONE);
                continue;
            }

            TextView tagV = (TextView)layout.getChildAt(i);
            tagV.setText(tags.get(i).getName());
            tagV.setVisibility(View.VISIBLE);
        }

        return layout;
    }
}
