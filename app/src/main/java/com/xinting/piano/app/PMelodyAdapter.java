package com.xinting.piano.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xinting.piano.R;
import com.xinting.piano.data.PMelody;

import java.util.List;

/**
 * Created by wuxinting on 15/10/28.
 *
 * 乐曲列表的适配器
 */
public class PMelodyAdapter extends ArrayAdapter<PMelody> {

    private LayoutInflater inflater;
    private static final int resource = R.layout.melody_item;

    private PTagAdapter tagAdapter;
    private PUserAdapter userAdapter;

    public PMelodyAdapter(Context context, List<PMelody> melodies) {
        super(context, resource, melodies);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public PTagAdapter getTagAdapter() {
        return tagAdapter;
    }

    public void setTagAdapter(PTagAdapter tagAdapter) {
        this.tagAdapter = tagAdapter;
    }

    public PUserAdapter getUserAdapter() {
        return userAdapter;
    }

    public void setUserAdapter(PUserAdapter userAdapter) {
        this.userAdapter = userAdapter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(resource, parent, false);
        } else {
            view = convertView;
        }

        TextView name = (TextView) view.findViewById(R.id.melody_name);
        TextView duration = (TextView) view.findViewById(R.id.melody_duration);

        name.setText(getItem(position).getTitle());
        duration.setText(getItem(position).getDurationString());

        if (tagAdapter != null) {
            tagAdapter.getView(getItem(position).getTags(),  (LinearLayout) view);
        }

        if (userAdapter != null) {
            userAdapter.getView(getItem(position).getUser(), (LinearLayout) view);
        }

        return view;
    }
}
