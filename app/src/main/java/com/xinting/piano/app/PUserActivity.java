package com.xinting.piano.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hannesdorfmann.swipeback.Position;
import com.hannesdorfmann.swipeback.SwipeBack;
import com.xinting.piano.R;
import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PColor;
import com.xinting.piano.data.PUser;
import com.xinting.piano.view.PUserMelodyFragment;
import com.xinting.piano.view.PUserProfileFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuxinting on 15/11/5.
 */
public class PUserActivity extends PBaseActivity {

    private PUser user;

    private TextView phoneView;
    private TextView nickView;

    /**
     * 滑动容器
     */
    private ViewPager pager;
    private List<Fragment> fragments;
    private PScreenSlideAdapter adapter;
    private int currentScreen;
    private int currentPosition;
    private int curretOffsetPixels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SwipeBack.attach(this, Position.LEFT)
            .setContentView(R.layout.activity_user)
            .setSwipeBackView(R.layout.swipeback_default)
            .setDividerAsSolidColor(PColor.SOFT_WHITE)
            .setDividerSize(2)
            .setOnInterceptMoveEventListener(new SwipeBack.OnInterceptMoveEventListener() {
                @Override
                public boolean isViewDraggable(View view, int i, int i1, int i2) {
                    if (pager != null && view == pager) {
                        return !(currentPosition == 0 && curretOffsetPixels == 0) || i < 0;
                    }
                    return false;
                }
            });

        Intent intent = getIntent();
        user = (PUser) intent.getSerializableExtra(PUser.KEY);

        fragments = new ArrayList<>();
        fragments.add(new PUserProfileFragment().setUser(user));
        //暂时不上评论这个功能
//        fragments.add(new PUserMessageFragment().setUser(user));
        fragments.add(new PUserMelodyFragment().setUser(user));

        pager = $(R.id.user_pager);
        adapter = new PScreenSlideAdapter(getSupportFragmentManager(), fragments);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new PageChangeListener());

        if (intent.hasExtra(PApp.KEY_INDEX)) {
            int index = intent.getIntExtra(PApp.KEY_INDEX, 0);
            pager.setCurrentItem(index);
        } else {
            int index = savedInstanceState == null ? 0 : savedInstanceState.getInt(PApp.KEY_INDEX, 0);
            pager.setCurrentItem(index);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PApp.KEY_INDEX, currentScreen);
    }

    /**
     * 滑动的监听
     */
    private class PageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            currentPosition = position;
            curretOffsetPixels = positionOffsetPixels;
        }

        @Override
        public void onPageSelected(int position) {
            currentScreen = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            Log.v(PApp.LOG_TAG, ""+state);
        }
    }

}
