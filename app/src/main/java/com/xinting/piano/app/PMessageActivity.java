package com.xinting.piano.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xinting.piano.R;
import com.xinting.piano.data.PApp;
import com.xinting.piano.data.PError;
import com.xinting.piano.data.PMessage;
import com.xinting.piano.data.PUser;
import com.xinting.piano.lib.util.PUtil;
import com.xinting.piano.service.IHandleJson;
import com.xinting.piano.service.PAccount;
import com.xinting.piano.service.PMessageService;
import com.xinting.piano.view.PAddControlView;
import com.xinting.piano.view.PControlView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wuxinting on 15/11/15.
 * 信件对话的界面
 */
public class PMessageActivity extends PBaseActivity {

    private PUser user;
    private int action = -1;

    //添加信件
    private PControlView addControlView;

    //添加信件的主体view
    private LinearLayout addLayout;
    //接收人
    private TextView toTextView;
    //发件人
    private TextView fromTextView;
    private EditText contentEditText;
    //确定发送
    private Button okBtn;

    private HandleMessageJson handleMessageJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        handleMessageJson = new HandleMessageJson();

        Intent intent = getIntent();
        if (intent.hasExtra(PUser.KEY)) {
            //目标用户
            user = (PUser) intent.getSerializableExtra(PUser.KEY);
        }

        if (intent.hasExtra(PApp.KEY_ACTION)) {
            action = intent.getIntExtra(PApp.KEY_ACTION, -1);
        }

        addControlView = $(R.id.message_add);
        addLayout = $(R.id.message_add_body);
        toTextView = $(R.id.message_add_body_to);
        fromTextView = $(R.id.message_add_body_from);
        contentEditText = $(R.id.message_add_body_content);
        okBtn = $(R.id.message_add_ok);

        addControlView.addCustomControlView(new PAddControlView());
        addControlView.setCustomType(PApp.CONTROL_VIEW_ADD);
        okBtn.setOnClickListener(new OnSendMessageClickListener());

        //非默认行为
        if (action == PMessage.ACTION_ADD) {
            addMessage();
        }
    }

    /**
     * 添加信件
     */
    private void addMessage() {
        addLayout.setVisibility(View.VISIBLE);
        toTextView.setText($s(R.string.user_message_to) + user.getNick());
        fromTextView.setText($s(R.string.user_message_from) + PAccount.getInstance().getCurrentUser().getNick());
        addControlView.setVisibility(View.GONE);
    }

    /**
     * 回默认界面
     */
    private void toMessageList() {
        addLayout.setVisibility(View.GONE);
        addControlView.setVisibility(View.VISIBLE);
    }

    /**
     * 发送信件
     */
    private class OnSendMessageClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            PMessageService messageService = new PMessageService();
            messageService.sendMessage(user, contentEditText.getText().toString(), handleMessageJson);
        }
    }

    /**
     * 处理业务返回
     */
    private class HandleMessageJson extends Handler implements IHandleJson {

        @Override
        public void handleJson(JSONObject json) {
            Message msg = Message.obtain();
            msg.obj = json;
            sendMessage(msg);
            Log.v(PMessageService.LOG_TAG, json.toString());
        }

        @Override
        public void handleMessage(Message msg) {
            JSONObject json = (JSONObject) msg.obj;
            int code = json.optInt(PError.KEY_CODE, PError.ERR_UNKNOWN);

            PUtil.toast(PApp.getAppContext(), json.optString(PError.KEY_MSG));
            if (code == PError.ERR_SUCCESS){
                if (action != -1) {
                    finish();
                } else {
                    toMessageList();
                }
            }
        }
    }
}
