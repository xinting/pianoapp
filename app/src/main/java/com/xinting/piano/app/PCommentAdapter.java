package com.xinting.piano.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xinting.piano.R;
import com.xinting.piano.data.PComment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by wuxinting on 15/11/8.
 */
public class PCommentAdapter extends ArrayAdapter<PComment> {

    //展示的资源
    private static final int resource = R.layout.comment_item;

    private LayoutInflater inflater;
    private PUserAdapter userAdapter;

    private DateFormat format;

    public PCommentAdapter(Context context, List<PComment> comments) {
        super(context, resource, comments);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        format = new SimpleDateFormat("ccc MMM dd kk:mm", Locale.ENGLISH);
    }

    public PUserAdapter getUserAdapter() {
        return userAdapter;
    }

    public void setUserAdapter(PUserAdapter userAdapter) {
        this.userAdapter = userAdapter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        if (convertView == null) {
            v = inflater.inflate(resource, parent, false);
        } else {
            v = convertView;
        }

        PComment comment = getItem(position);
        TextView content = (TextView) v.findViewById(R.id.comment_content);
        content.setText(comment.getContent());

        TextView date = (TextView) v.findViewById(R.id.comment_date);
        date.setText(format.format(comment.getDate()));

        if (userAdapter != null) {
            userAdapter.getView(comment.getUser(), (LinearLayout) v);
        }

        return v;
    }

}
