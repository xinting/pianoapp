package com.xinting.piano.app;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xinting.piano.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuxinting on 15/12/16.
 * 应用中的所有dialog原型
 */
public class PDialog extends Dialog {

    /**
     * 新建对话框要确定的类型
     */
    public enum Type {
        ALERT, // 确认对话框，有两个按钮，“确定”，“取消”
    }

    private Context context;
    private LinearLayout header;
    private ImageView icon;
    private TextView title;
    private TextView body;
    private LinearLayout footer;
    private List<Button> btns;

    // 保存该弹出框的结果，OK, CANCEL, 。。
    private int result;

    public PDialog(Context context) {
        super(context);
        this.context = context;

        // 使用自定义的标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog, null, false);
        header = (LinearLayout) v.findViewById(R.id.dialog_header);
        body = (TextView) v.findViewById(R.id.dialog_body);
        footer = (LinearLayout) v.findViewById(R.id.dialog_footer);

        icon = (ImageView) v.findViewById(R.id.dialog_header_icon);
        title = (TextView) v.findViewById(R.id.dialog_header_title);

        btns = new ArrayList<>();
        for (int i = 0; i < footer.getChildCount(); i++) {
            btns.add((Button) footer.getChildAt(i));
        }
        setContentView(v);

        initialize();
    }

    private void initialize() {
        // 调整宽度
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();

        WindowManager manager = window.getWindowManager();
        Display display = manager.getDefaultDisplay();

        lp.width = (int) (display.getWidth() * 0.6f);
        window.setAttributes(lp);

        // ok
        btns.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        // cancel
        btns.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    /**
     * 设置确定回调
     * @param listener
     * @return
     */
    public PDialog setOnPositiveListener(View.OnClickListener listener) {
        btns.get(0).setOnClickListener(listener);
        return this;
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title.setText(title);

        this.title.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTitle(int titleId) {
        title.setText(titleId);

        title.setVisibility(View.VISIBLE);
    }

    /**
     * 设置icon
     * @param resId icon id
     */
    public void setIcon(int resId) {
        icon.setImageResource(resId);

        icon.setVisibility(View.VISIBLE);
    }

    @Override
    public void setCancelable(boolean flag) {
        if (flag) {
            btns.get(1).setVisibility(View.VISIBLE);
        } else {
            btns.get(1).setVisibility(View.GONE);
        }
    }

    /**
     * 设置内容
     * @param content
     */
    public void setContent(CharSequence content) {
        body.setText(content);

        body.setVisibility(View.VISIBLE);
    }

    public void setContent(int resId) {
        body.setText(resId);

        body.setVisibility(View.VISIBLE);
    }

    /**
     * 显示头部
     * @param visibility
     */
    public void showHeader(int visibility) {
        header.setVisibility(visibility);
    }

    /**
     * 显示内容
     * @param visibility
     */
    public void showBody(int visibility) {
        body.setVisibility(visibility);
    }

    /**
     * 显示脚部
     * @param visibility
     */
    public void showFooter(int visibility) {
        footer.setVisibility(visibility);
    }

}
