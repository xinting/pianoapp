package com.xinting.piano.service;

import org.json.JSONObject;

/**
 * Created by wuxinting on 15/11/5.
 * service 统一回调接口
 */
public interface IHandleJson {

    /**
     * 处理业务返回的json, 可能为Null
     * @param json
     */
    void handleJson(JSONObject json);
}
