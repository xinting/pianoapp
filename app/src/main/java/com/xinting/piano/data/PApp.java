package com.xinting.piano.data;

import android.content.Context;

/**
 * Created by wuxinting on 15/11/5.
 * config about application
 */
public class PApp {

    public static final String URL_SERVER = "http://123.57.65.0/";

    public static final String LOG_TAG = "piano";

    //error
    public static final String ERR_CODE = "code";
    public static final String ERR_MSG = "msg";
    public static final int ERR_UNKNOWN = -1;
    public static final int ERR_SUCCESS = 0;
    //用户不存在
    public static final int ERR_VOID_USER = 7;

    //通用的字典KEY
    //进度
    public static final String KEY_PROCESS = "process";
    public static final String KEY_PROCESS_AT = "process_at";
    public static final String KEY_PROCESS_RANGE = "process_range";

    public static final String KEY_COUNT = "count";
    public static final String KEY_INDEX = "index";
    public static final String KEY_ID = "id";
    //进入到新页面时的默认行为，由跳转前页面指定
    public static final String KEY_ACTION = "action";
    public static final String KEY_CONTENT = "content";
    //进入到新页面时的传入，当新页面finish时跳转去向，主要用于登录注册页
    public static final String KEY_NEXT = "next";
    public static final String KEY_VERSIONCODE = "versioncode";
    public static final String KEY_VERSION = "version";
    public static final String KEY_URL = "url";
    public static final String KEY_DATE = "date";
    public static final String KEY_MORE = "more";

    //routine task
    public static final int ROUTINE_MESSAGE = 0;
    public static final int ROUTINE_ACCOUNT = 1;
    public static final int ROUTINE_UPGRADE = 2;
    public static final int ROUTINE_ANNOUNCE = 3;

    //control view type
    public static final int CONTROL_VIEW_COMMENT = 1;
    public static final int CONTROL_VIEW_USER = 2;
    public static final int CONTROL_VIEW_MESSAGE = 3;
    public static final int CONTROL_VIEW_ADD = 4;
    public static final int CONTROL_VIEW_LIKE = 5;

    //broadcast id (arg1)
    //login success
    public static final int BC_ACCOUNT_STATUS_CHANGE = 1;
    //invalid token, need login with username&password
    public static final int BC_ACCOUNT_INVALID_TOKEN = 2;
    //网络错误
    public static final int BC_NETWORK_ERROR = 3;

    //share preferences id
    public static final String KEY_SP_LIKE = "sp_like";

    /**
     * the unique context of piano application
     */
    private static Context context;
    public static Context getAppContext() {
        return context;
    }
    public static void setAppContext(Context context) {
        PApp.context = context;
    }


}
