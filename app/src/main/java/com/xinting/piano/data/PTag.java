package com.xinting.piano.data;

import java.io.Serializable;

/**
 * Created by wuxinting on 15/10/28.
 *
 * 标签类，标识乐曲等等
 */
public class PTag implements Serializable {

    public static final String KEY = "tag";
    public static final String KEY_S = "tags";

    private String name;

    public PTag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
