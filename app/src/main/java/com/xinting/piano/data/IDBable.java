package com.xinting.piano.data;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by wuxinting on 15/11/28.
 * 可使用SQLite存储的接口
 */
public interface IDBable {

    /**
     * 一个数据的数据表描述
     * @return
     */
    String getDBDesc();

    /**
     * 根据游标生成自身
     * @param cursor
     */
    void fromCursor(Cursor cursor);

    /**
     * 转化成可被数据库存储的格式
     * @return
     */
    ContentValues toContentValues();
}
