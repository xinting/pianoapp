package com.xinting.piano.data;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by wuxinting on 15/11/13.
 * 私信实体
 */
public class PMessage implements IJsonable {

    public static final String LOG_TAG = PApp.LOG_TAG+"_"+PMessage.KEY;
    public static final String KEY = "message";
    public static final String KEY_S = "messages";

    public static final int ACTION_ADD = 0;

    private String content;
    private PUser to;
    private PUser from;
    private Date date;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public PUser getTo() {
        return to;
    }

    public void setTo(PUser to) {
        this.to = to;
    }

    public PUser getFrom() {
        return from;
    }

    public void setFrom(PUser from) {
        this.from = from;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public void fromJson(JSONObject json) {

    }
}
