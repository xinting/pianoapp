package com.xinting.piano.data;

import android.graphics.Color;

/**
 * Created by wuxinting on 15/10/30.
 */
public class PColor {

    public static final int METAL_BLACK = Color.rgb(34, 34, 34);
    public static final int METAL_RED = Color.rgb(134, 5, 8);
    public static final int SOFT_WHITE = Color.rgb(234, 234, 234);
}
