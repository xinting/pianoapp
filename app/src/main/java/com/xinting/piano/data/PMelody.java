package com.xinting.piano.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wuxinting on 15/10/28.
 *
 * 乐曲的实体类
 */
public class PMelody implements Serializable, IJsonable, IDBable {

    public static final String LOG_TAG = PApp.LOG_TAG+"_melody";
    public static final String KEY = "melody";
    public static final String KEY_S = "melodies";
    public static final String KEY_DURATION = "duration";
    public static final String KEY_TITLE = "title";
    public static final String KEY_LIKE = "like";

    public static final String MELODY_SUFFIX = ".3gp";
    public static final String TABLE_NAME = KEY;

    /**
     * 乐曲ID
     */
    private String id;

    /***
     * 乐曲的名称
     */
    private String title;

    /**
     * 乐曲持续的时间
     */
    private int duration;

    /**
     * 喜欢的个数
     */
    private long like;

    /**
     * 发布的日期
     */
    private Date date;

    /**
     * 上传该乐曲的用户
     */
    private PUser user;

    /**
     * 该乐曲的标签。如 哈农练习曲一，其标签可能包括"哈农"
     */
    private List<PTag> tags;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public String getDurationString() {
        int m = duration / 60;
        int s = duration % 60;
        return "" + m + "'" + s + "\"";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public long getLike() {
        return like;
    }

    public void setLike(long like) {
        this.like = like;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public PUser getUser() {
        return user;
    }

    public void setUser(PUser user) {
        this.user = user;
    }

    public List<PTag> getTags() {
        return tags;
    }

    public void setTags(List<PTag> tags) {
        this.tags = tags;
    }

    public void addTag(PTag tag) {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        tags.add(tag);
    }

    public void addTags(List<PTag> tags) {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        tags.addAll(tags);
    }

    @Override
    public void fromJson(JSONObject json) {
        setLike(json.optLong(KEY_LIKE, 0L));

        String dateStr = json.optString(PApp.KEY_DATE, "");
        Date date = new Date(dateStr);
        setDate(date);

        try {
            if (json.has(KEY_DURATION)) {
                setDuration(json.getInt(KEY_DURATION));
            }

            if (json.has(KEY_TITLE)) {
                String title = json.getString(KEY_TITLE);
                title = URLDecoder.decode(title, "UTF-8");
                setTitle(title);
            }

            if (json.has(PApp.KEY_ID)) {
                setId(json.getString(PApp.KEY_ID));
            }

            if (json.has(PUser.KEY)) {
                PUser user = new PUser();
                user.fromJson(json.getJSONObject(PUser.KEY));
                setUser(user);
            }

            if (json.has(PTag.KEY_S)) {
                JSONArray tags = json.getJSONArray(PTag.KEY_S);
                for (int i = 0; i < tags.length(); i++) {
                    PTag tag = new PTag(tags.getString(i));
                    addTag(tag);
                }
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    @Override
    public String getDBDesc() {
        StringBuilder sb = new StringBuilder();
        sb.append(TABLE_NAME +'(');
        sb.append(PApp.KEY_ID+" integer primary key,");
        sb.append(KEY_TITLE+" varchar,");
        sb.append(KEY_DURATION+" integer,");
        sb.append(KEY_LIKE+" integer,");
        sb.append(PApp.KEY_DATE+" time,");
        sb.append(PUser.KEY+" varchar");
        sb.append(")");

        return sb.toString();
    }

    @Override
    public void fromCursor(Cursor cursor) {
        if (cursor == null) {
            return;
        }

        id = ""+cursor.getInt(cursor.getColumnIndex(PApp.KEY_ID));
        title = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
        duration = cursor.getInt(cursor.getColumnIndex(KEY_DURATION));
        like = cursor.getInt(cursor.getColumnIndex(KEY_LIKE));

        PUser user = new PUser();
        user.setNick(cursor.getString(cursor.getColumnIndex(PUser.KEY)));
        setUser(user);
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(PApp.KEY_ID, id);
        cv.put(KEY_TITLE, title);
        cv.put(KEY_DURATION, duration);
        cv.put(KEY_LIKE, like);
        cv.put(PApp.KEY_DATE, date.toString());
        cv.put(PUser.KEY, user.getNick());
        return cv;
    }
}
