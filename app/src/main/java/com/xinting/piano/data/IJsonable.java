package com.xinting.piano.data;

import org.json.JSONObject;

/**
 * Created by wuxinting on 15/11/7.
 * 与json相互转化的接口
 */
public interface IJsonable {

    void fromJson(JSONObject json);

}
