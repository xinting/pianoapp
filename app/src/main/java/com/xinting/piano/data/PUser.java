package com.xinting.piano.data;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by wuxinting on 15/10/28.
 *
 * 用户实体类
 */
public class PUser implements Serializable, IJsonable {

    public static final String LOG_TAG = PApp.LOG_TAG+"_user";
    public static final String KEY = "user";
    public static final String KEY_PHONE = "username";
    public static final String KEY_NICK = "nickname";

    private String phone;
    private String nick;
    private String id;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void fromJson(JSONObject json) {
        try {
            if (json.has(PApp.KEY_ID)) {
                setId(json.getString(PApp.KEY_ID));
            }

            if (json.has(KEY_PHONE)) {
                setPhone(json.getString(KEY_PHONE));
            }

            String nick = json.optString(KEY_NICK, "");
            nick = URLDecoder.decode(nick, "UTF-8");
            setNick(nick);

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            Log.e(LOG_TAG, "Decode nick failed. ");
        }
    }
}
