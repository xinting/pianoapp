package com.xinting.piano.data;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by wuxinting on 15/11/28.
 * 统一错误，之前的方式暂时不改，慢慢重构
 */
public class PError implements Serializable, IJsonable{

    public static final String KEY_CODE = "code";
    public static final String KEY_MSG = "msg";

    public static final int ERR_NOT_NICK = -5;
    public static final int ERR_NOT_VERIFY = -4;
    public static final int ERR_NOT_PASSWORD = -3;
    public static final int ERR_NOT_PHONE = -2;
    public static final int ERR_UNKNOWN = -1;
    public static final int ERR_SUCCESS = 0;
    //不能重复的行为，重复发生的错误号
    public static final int ERR_REPEAT = 1;
    //验证码错误
    public static final int ERR_VERIFY_INCORRECT = 2;
    //用户名密码不匹配
    public static final int ERR_AUTH_FAIL = 3;
    public static final int ERR_INVALID_TOKEN = 5;
    //用户不存在
    public static final int ERR_VOID_USER = 7;
    //验证码已经发送
    public static final int ERR_VERIFY_SENT = 9;
    public static final int ERR_REPEAT_NICK = 10;
    public static final int ERR_VERIFY_SEND_FAIL = 12;

    public static final String ERR_MSG_UNKNOWN = "我也不知道出了什么情况..";

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return getCode() == ERR_SUCCESS;
    }

    @Override
    public void fromJson(JSONObject json) {
        code = json.optInt(KEY_CODE, ERR_UNKNOWN);
        msg = json.optString(KEY_MSG, ERR_MSG_UNKNOWN);
    }
}
