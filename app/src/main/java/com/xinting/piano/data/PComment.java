package com.xinting.piano.data;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by wuxinting on 15/11/8.
 * 对乐曲的评论实体
 */
public class PComment implements IJsonable {

    public static final String LOG_TAG = PApp.LOG_TAG+"_comment";
    //用于json的Key
    public static final String KEY_MELODY = "album";
    public static final String KEY_USER = "user";
    public static final String KEY_DATE = "date";
    public static final String KEY_CONTENT = "content";

    /**
     * 评论的对象
     */
    private PMelody melody;

    /**
     * 评论者
     */
    private PUser user;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评论的时间
     */
    private Date date;

    public PMelody getMelody() {
        return melody;
    }

    public void setMelody(PMelody melody) {
        this.melody = melody;
    }

    public PUser getUser() {
        return user;
    }

    public void setUser(PUser user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public void fromJson(JSONObject json) {
        try {
            setContent(json.getString(KEY_CONTENT));

            PMelody melody = new PMelody();
            melody.fromJson(json.getJSONObject(KEY_MELODY));
            setMelody(melody);

            setDate(new Date(json.getString(KEY_DATE)));

            PUser user = new PUser();
            user.setNick(json.getString(KEY_USER));
            setUser(user);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage() + ":" + json.toString());
        }
    }
}
