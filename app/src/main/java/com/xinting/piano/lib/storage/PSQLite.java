package com.xinting.piano.lib.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.xinting.piano.data.PApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuxinting on 15/11/28.
 * 封装所有数据库盯着操作
 */
public class PSQLite extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "pianodb";

    private String name;
    private Context context;
    //默认的表名
    private String table = "main";
    private SQLiteDatabase db;

    public PSQLite(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        initialize(context, DB_NAME);
    }

    public PSQLite(Context context, String dbName) {
        super(context, dbName, null, DB_VERSION);

        initialize(context, dbName);
    }

    private void initialize(Context context, String dbName) {
        db = context.openOrCreateDatabase(dbName, Context.MODE_PRIVATE, null);
    }

    /**
     * 选择一张表操作
     * @param table 表名
     * @param tableDesc 表描述，如果在表不存在时使用, 如 melody (_id INTEGER PRIMARY KEY AUTOINCREMENT, title varchar, ... )
     * @return
     */
    public boolean selectTable(String table, @Nullable String tableDesc) {
        this.table = table;

        if (tableDesc != null) {
            String sql = "create table if not exists " + tableDesc;
            Log.e("piano", sql);
            if (db != null) {
                db.execSQL(sql);
            }
        }
        return true;
    }

    /**
     * 删除表
     */
    public void dropTable() {
        String sql = "drop table " + table;
        if (db != null) {
            db.execSQL(sql);
        }
    }

    /**
     * 插入数据
     * @param cv
     * @return
     */
    public long insertData(ContentValues cv) {
        if (db != null) {
            return db.insert(table, null, cv);
        }

        return 0;
    }

    /**
     * 更新数据，不需要更新的部分直接设为Null
     * @param cv
     * @param whereClause 条件判断 如 title = ?
     * @param whereArgs 条件判断值 如 "haha"
     * @return
     */
    public int updateData(ContentValues cv, String whereClause, String[] whereArgs) {
        if (db != null) {
            return db.update(table, cv, whereClause, whereArgs);
        }
        return 0;
    }

    /**
     * 删除满足某一条件的行
     * @param whereClause
     * @param whereArgs
     * @return
     */
    public int delete(String whereClause, String[] whereArgs) {
        if (db != null) {
            return db.delete(table, whereClause, whereArgs);
        }
        return 0;
    }

    /**
     * 返回sql执行结果
     * @param sql
     * @param args 查询的参数
     * @return 游标
     */
    public Cursor query(String sql, String[] args) {
        if (db != null) {
            return db.rawQuery(sql, args);
        }
        return null;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
