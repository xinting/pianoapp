package com.xinting.piano.lib.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

/**
 * Created by wuxinting on 15/11/5.
 */
public class PSharePreferences {

    private Context context;
    private SharedPreferences sp;

    public static final String TAG = "piano";

    /**
     * 如果想使用自定义特定名称的Shared Preferences 请调用{@link #openSharedPreferences(String)}
     * @param context
     */
    public PSharePreferences(Context context) {
        this.context = context;
    }

    public PSharePreferences openSharedPreferences(String sp) {
        this.sp = context.getSharedPreferences(sp, Context.MODE_PRIVATE);
        return this;
    }

    public SharedPreferences getSharedPreferences() {
        if (sp == null) {
            openSharedPreferences(TAG);
        }
        return sp;
    }

    /**
     * save value of key into Share preferences sp
     * @param sp share preference name
     * @param key key
     * @param value value
     */
    public <T> void save(String sp, String key, T value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sp, Context.MODE_PRIVATE);
        save(sharedPreferences, key, value);
    }

    public <T> void save(String key, T value) {
        if (sp == null) {
            openSharedPreferences(TAG);
        }
        save(sp, key, value);
    }

    /**
     * save value of key
     * @param key
     * @param value
     */
    private <T> void save(SharedPreferences sp, String key, T value) {
        SharedPreferences.Editor editor = sp.edit();
        if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else {
            throw new IllegalArgumentException();
        }
        editor.commit();
    }

    /**
     * 获取TAG 对应share preferences 中key中的value
     * @param key
     * @param <T>
     * @return
     */
    public <T> T get(String key) {
        if (sp == null) {
            openSharedPreferences(TAG);
        }

        return get(sp, key);
    }

    /**
     * Get All Contents
     * @return
     */
    public Map<String, ?> getAll() {
        if (sp == null) {
            openSharedPreferences(TAG);
        }

        return sp.getAll();
    }

    /**
     * Clear Shared preferences
     */
    public void clear() {
        if (sp == null) {
            openSharedPreferences(TAG);
        }
        sp.edit().clear().commit();
    }

    /**
     * 获取TAG对应share preferences中key的value
     * @param key
     * @param origin 如果没有值，返回该值
     * @param <T>
     * @return
     */
    public <T> T get(String key, T origin) {
        Object obj = get(key);
        if (obj == null) {
            obj = origin;
        }
        return (T) obj;
    }

    /**
     * 获取share preferences sp 中key中的值
     * @param sp
     * @param key
     * @param origin 如果获取内容是Null, 返回origin
     * @param <T>
     * @return
     */
    public <T> T get(String sp, String key, T origin) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sp, Context.MODE_PRIVATE);
        T obj = get(sharedPreferences, key);
        if (obj == null) {
            obj = origin;
        }
        return obj;
    }

    private <T> T get(SharedPreferences sp, String key) {
        if (sp == null) {
            return null;
        }
        for (Map.Entry entry : sp.getAll().entrySet()) {
            if (entry.getKey().equals(key)) {
                try {
                    T rs = (T) entry.getValue();
                    return rs;
                } catch (Exception e) {}
            }
        }
        return null;
    }
}
