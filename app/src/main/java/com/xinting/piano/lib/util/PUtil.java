package com.xinting.piano.lib.util;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.xinting.piano.data.PApp;

import java.lang.ref.SoftReference;
import java.util.regex.Pattern;

/**
 * Created by wuxinting on 15/11/5.
 * utilities
 */
public class PUtil {

    /**
     * check the phone number right or not
     * @param phone phone number
     * @return
     */
    public static boolean checkPhoneNumber(String phone) {
        Pattern p = Pattern.compile("^1[3,4,5,7,8][0-9]{9}$");
        return p.matcher(phone).matches();
    }

    /**
     * check the password legal or not
     * 16 < > 8
     * letter number special
     * @param passwd
     * @return
     */
    public static boolean checkPasswd(String passwd) {
        Pattern p = Pattern.compile("^[\\w\\d`~!@#$%^&*()]{8,16}$");
        return p.matcher(passwd).matches();
    }

    /**
     * 检查用户名
     * @param nick
     * @return
     */
    public static boolean checkNickName(String nick) {
        Pattern p = Pattern.compile("^\\w{2,10}$");
        return p.matcher(nick).matches();
    }

    /**
     * 检查验证码
     * @param code 验证码
     * @return true | false
     */
    public static boolean checkVerifyCode(String code) {
        Pattern p = Pattern.compile("^\\d{4}$");
        return p.matcher(code).matches();
    }

    /**
     * join urls
     * @param urls
     * @return
     */
    public static String joinURL(String... urls) {
        StringBuilder sb = new StringBuilder();
        for (String url : urls) {
            sb.append(url);
            while (sb.charAt(sb.length()-1) == '/') {
                sb.deleteCharAt(sb.length()-1);
            }
            sb.append('/');
        }
        return sb.toString();
    }

    /**
     * show toast, only one in the same time
     * @param context
     * @param msg
     */
    private static SoftReference<Toast> mToast = null;
    public static void toast(Context context, CharSequence msg) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }

        if (mToast != null && mToast.get() != null) {
            mToast.get().cancel();
        }

        mToast = new SoftReference<>(Toast.makeText(context, msg, Toast.LENGTH_LONG));
        mToast.get().show();
    }

    /**
     * show or hide soft input
     * @param v the view bind the input
     * @param bShow if is true, show the input, else hide it
     * @return if operate success, return true, else return false
     */
    public static boolean showSoftInput(View v, boolean bShow) {
        InputMethodManager manager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!bShow) {
            return manager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } else {
            return manager.showSoftInput(v, InputMethodManager.SHOW_FORCED);
        }
    }

    /**
     * toggle the input
     * @param v
     */
    public static void toggleSoftKeyboard(View v) {
        InputMethodManager manager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
